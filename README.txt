Hide Super User
===============

Introduction
------------
This module helps to hide the Super user account from the People listing page.
The main objective is to restrict access from any other Administrator, to edit
or block the Super user account.

The Super user account can be used for enabling or disabling modules and making 
configuration changes, which might be sensitive to any application. In case, 
such tasks may not be allowed to be done by other CMS administrators, then this
module will be helpful.


Requirements
------------
This module is created to work with Drupal 7.x version.


Installation
------------
1. Download the Hide Super User module from drupal.org
2. Go to the Modules listing page at /admin/modules
3. Enable the Hide Super User module
4. Visit the People listing page at /admin/people to check
    if the sure user is now hiden from the list.


Configuration
-------------
There is no configuration needed for this module


FAQ
---
Q: 	Why will you use this module?

A: 	This module will be used to hide the super user account. 
	It will be useful if you do not wish to disclose the 
        existence of such a user who has all the privileges compared 
        to the other Administrators within the CMS. 
	This way the other Administrators who have the Administer users 
        permission, will not have the option to edit or block the super user.
	

Q: 	Will there be any database update done to hide the super user?

A: 	No, there is no database update done via this module. 
	Hence it will have no impact on any of the properties or attributes 
        of the Super user account.
	It only hides the user from getting listed in the People's page


Q: 	Will the super user account become un-accessible after enabling this module?

A:	The super user can login to the system as is and can perform all the 
        tasks as expected by that user account.
	There is no change done to the super user, except hidding the account 
        from the users list.


Maintainers
-----------
Current maintainers:
	Ayan Dasgupta (ayan_d) - https://www.drupal.org/u/ayan_d
